# GIT Practice

## Description

This is a small project created for learning new concepts and getting familiar 
with GitLab.

## Contributors

This project is being created by [Javier Riveros](https://gitlab.com/javierriveros/) and [Jonathan Garzón](https://gitlab.com/jonathanGR3450)